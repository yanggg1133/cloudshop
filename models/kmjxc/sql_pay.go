package kmjxc

import (
	"database/sql"
	"strings"
	"time"

	_ "github.com/alexbrainman/odbc"
	"github.com/astaxie/beego/orm"
	"github.com/axgle/mahonia"
)

//RpTRecpayRecordItem 付款单明细
type RpTRecpayRecordItem struct {
	Master      *RpTRecpayRecordInfo //主表
	SettleNo    string               //单据编号
	SettleDate  time.Time            //单据日期
	SheetAmount float64              //单据总金额
	PaidAmount  float64              //付款金额
	PaidDate    time.Time            //付款日期
	Other1      string               //其它1
	Other2      string               //其它2
	Other3      string               //其它3
	DiscAmt     float64              //未知
}

//RpTRecpayRecordInfo 供应商付款单
type RpTRecpayRecordInfo struct {
	Supcust     *BiTSupcustInfo        //供应商信息
	SheetNo     string                 //付款单编号
	OperName    string                 //操作员
	OperDate    time.Time              //操作日期
	ApproveFlag bool                   //未知
	PrintTimes  int                    //打印次数
	Detail      []*RpTRecpayRecordItem //付款单据明细
	FeeDetail   []SupplierFeeItem      //扣款费用明细
}

//GetRpTRecpayRecordInfoListBySupcustNo 通过供应商编号获取付款列表
func GetRpTRecpayRecordInfoListBySupcustNo(supcustNo string) []*RpTRecpayRecordInfo {
	var list []*RpTRecpayRecordInfo
	var supcust *BiTSupcustInfo
	supcust = GetBiTSupcupstInfo(supcustNo)[0]

	conn, err := sql.Open("odbc", KMJXCConnStr)
	if err != nil {
		panic(err)
	}
	defer conn.Close()
	stmt, err := conn.Prepare(` SELECT distinct rp_t_recpay_record_info.sheet_no,sa_t_operator_i.oper_name,oper_date, approve_flag    FROM rp_t_recpay_record_info join sa_t_operator_i on sa_t_operator_i.oper_id = rp_t_recpay_record_info.oper_id where  (substring(rp_t_recpay_record_info.sheet_no,1,2)='RP')  and (rp_t_recpay_record_info. sheet_No  like '%')  and  supcust_no like ?  and (approve_flag like '%')  and (rp_t_recpay_record_info.oper_id like '%')  order by rp_t_recpay_record_info.sheet_no `)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	row, _ := stmt.Query(supcustNo)
	defer row.Close()
	dec := mahonia.NewDecoder("gbk")
	for row.Next() {
		var info RpTRecpayRecordInfo
		info.Supcust = supcust
		row.Scan(&info.SheetNo, &info.OperName, &info.OperDate, &info.ApproveFlag)
		info.SheetNo = strings.TrimSpace(info.SheetNo)
		info.OperName = dec.ConvertString(info.OperName)
		list = append(list, &info)
	}
	return list
}

//GetRpTRecpayRecordInfoBySheetNo 获取付款汇付单明细
func GetRpTRecpayRecordInfoBySheetNo(sheetNo string) RpTRecpayRecordInfo {
	var info RpTRecpayRecordInfo
	var supcust *BiTSupcustInfo

	conn, err := sql.Open("odbc", KMJXCConnStr)
	if err != nil {
		panic(err)
	}
	defer conn.Close()
	stmt, err := conn.Prepare(`SELECT info.settle_no,info.supcust_no,oper.oper_name,info.oper_date,info.approve_flag,info.settle_date,isnull(info.sheet_amount*pay_type, 0.00)as sheet_amount,info.other1,   info.other2,   info.other3,   info.print_time, isnull ( flow.paid_amount * pay_type , 0.00 ) as paid_amount , pay_date FROM rp_t_recpay_record_info info,sa_t_operator_i oper,rp_t_accout_payrec_flow flow WHERE info.sheet_no like ? and info.oper_id = oper.oper_id and info.settle_no = flow.voucher_no`)
	defer stmt.Close()
	row, _ := stmt.Query(sheetNo)
	defer row.Close()

	o := orm.NewOrm()
	qs := o.QueryTable("supplier_fee_item")
	qs = qs.Filter("SheetNo", sheetNo)
	_, err = qs.All(&info.FeeDetail)
	if err != nil {
		panic(err)
	}

	var supcustNo string
	var operName string
	var operDate time.Time
	var approveFlag bool
	var printTimes int
	for row.Next() {
		var item RpTRecpayRecordItem
		row.Scan(&item.SettleNo, &supcustNo, &operName, &operDate, &approveFlag, &item.SettleDate, &item.SheetAmount, &item.Other1, &item.Other2, &item.Other3, &printTimes, &item.PaidAmount, &item.PaidDate)
		info.Detail = append(info.Detail, &item)
	}

	supcust = GetBiTSupcupstInfo(supcustNo)[0]
	dec := mahonia.NewDecoder("gbk")
	info.Supcust = supcust
	info.SheetNo = sheetNo
	info.OperName = dec.ConvertString(operName)
	info.OperDate = operDate
	info.ApproveFlag = approveFlag
	info.PrintTimes = printTimes
	return info
}
