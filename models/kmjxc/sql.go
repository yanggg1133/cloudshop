package kmjxc

import (
	"database/sql"
	"strconv"

	_ "github.com/alexbrainman/odbc"
	"github.com/astaxie/beego/orm"
	"github.com/axgle/mahonia"
)

var kmjxcSubIds map[string]int

func init() {
	orm.RegisterModel(new(SupplierFeeItem))

}

func initSubIds() {
	//init kmjxc_sub_ids
	o := orm.NewOrm()
	r := o.Raw(`select * from kmjxc_sub_numbers`)
	type subNo struct {
		ID    int
		Name  string
		SubNo int
	}
	var subs []subNo
	_, err := r.QueryRows(&subs)
	if err != nil {
		panic(err)
	}
	kmjxcSubIds = make(map[string]int)
	for _, sub := range subs {
		kmjxcSubIds[sub.Name] = sub.SubNo + 1
	}
}

const (
	//KMJXCConnStr 科脉进销存连接字符串
	KMJXCConnStr = "DSN=mssql;UID=km;PWD=kmtech"
)

const (
	//KMJXCFeeSubID 科脉进销存费用id
	KMJXCFeeSubID = "kmjxc_fee_sub_id"
)

//GetSubNo 获取id
func GetSubNo(idName string) string {
	if kmjxcSubIds == nil {
		initSubIds()
	}
	var idPref string
	ids := strconv.Itoa(kmjxcSubIds[idName])
	if kmjxcSubIds[idName] < 10 {
		idPref = "000"
	} else if kmjxcSubIds[idName] < 100 {
		idPref = "00"
	} else if kmjxcSubIds[idName] < 1000 {
		idPref = "0"
	} else if kmjxcSubIds[idName] < 10000 {
		idPref = ""
	} else {
		kmjxcSubIds[idName] = 1
		idPref = "000"
	}
	o := orm.NewOrm()
	r := o.Raw(`update kmjxc_sub_numbers set sub_no =` + ids + ` where name = "` + idName + `"`)
	_, err := r.Exec()
	if err != nil {
		panic(err)
	}
	kmjxcSubIds[idName]++
	return idPref + ids
}

//BiTSupcustInfo 供应商
type BiTSupcustInfo struct {
	SupcustNo      string                 //供应商编号
	SupName        string                 //供应商名称
	RegionName     string                 //所属地区
	SupMan         string                 //供应商联系人
	SupTel         string                 //供应商联系方式
	SupCashBankMan string                 //供应商银行开户名
	SupCashBankNo  string                 //供应商银行账号
	PaymentList    []*RpTRecpayRecordInfo //供应商付款明细
}

//GetBiTSupcupstInfo 获取供应商信息
func GetBiTSupcupstInfo(supcustNo string) []*BiTSupcustInfo {
	var infos []*BiTSupcustInfo
	conn, err := sql.Open("odbc", KMJXCConnStr)
	if err != nil {
		panic(err)
	}
	defer conn.Close()

	stmt, err := conn.Prepare(`select supcust_no,sup_name,region_name,sup_man,sup_tel,sup_cash_bankman,sup_cash_bankno from bi_t_supcust_info join bi_t_region_info on bi_t_supcust_info.region_no = bi_t_region_info.region_no where supcust_no like ?`)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	row, _ := stmt.Query(supcustNo)
	defer row.Close()
	values := make([]sql.RawBytes, 7)
	scanArgs := make([]interface{}, len(values))
	for i := range values {
		scanArgs[i] = &values[i]
	}
	dec := mahonia.NewDecoder("gbk")
	for row.Next() {
		var info BiTSupcustInfo
		row.Scan(scanArgs...)
		infoStr := make([]string, 7)
		for i := range infoStr {
			infoStr[i] = dec.ConvertString(string(values[i]))
		}
		info.SupcustNo = infoStr[0]
		info.SupName = infoStr[1]
		info.RegionName = infoStr[2]
		info.SupCashBankMan = infoStr[5]
		info.SupCashBankNo = infoStr[6]
		info.SupMan = infoStr[3]
		info.SupTel = infoStr[4]
		infos = append(infos, &info)
	}
	return infos
}
