package kmjxc

import (
	"time"

	"github.com/astaxie/beego/orm"
)

//SupplierFeeItem 费用明细
type SupplierFeeItem struct {
	FeeNo      string    `orm:"pk"`                     //FE费用编码
	FeeAmount  float64   `orm:"digits(10);decimals(2)"` //费用金额
	SupcustNo  string    //所属供应商
	FeeDate    time.Time `orm:"type(date)"`             //费用所属日期
	PaidDate   time.Time `orm:"type(date)"`             //费用扣取日期
	PaidAmount float64   `orm:"digits(10);decimals(2)"` //已扣取金额
	FeeBrief   string    //费用信息
	SignName   string    //申请人
	SheetNo    string    //所属付款单
}

//AddFeeOnRpTRecpayRecordInfo 保存费用更改
func AddFeeOnRpTRecpayRecordInfo(feeDate time.Time, feeBrief string, feeAmount float64, signName string, sheetNo string, supcustNo string, dept string) (string, error) {
	item := SupplierFeeItem{
		FeeDate:   feeDate,
		FeeBrief:  feeBrief,
		FeeAmount: feeAmount,
		SignName:  signName,
		SheetNo:   sheetNo,
		SupcustNo: supcustNo,
		//todo
		PaidAmount: feeAmount,
	}
	o := orm.NewOrm()
	item.FeeNo = "FE" + dept + time.Now().Format("060102") + GetSubNo(KMJXCFeeSubID)
	_, err := o.Insert(&item)
	return item.FeeNo, err
}

//UpdateFeeByFeeNo 更新费用
func UpdateFeeByFeeNo(feeNo string, feeDate time.Time, feeBrief string, feeAmount float64, signName string) error {
	item := SupplierFeeItem{FeeNo: feeNo}
	o := orm.NewOrm()
	err := o.Read(&item)
	if err != nil {
		return err
	}
	item.FeeDate = feeDate
	item.FeeBrief = feeBrief
	item.FeeAmount = feeAmount
	item.SignName = signName
	//todo
	item.PaidAmount = feeAmount
	_, err = o.Update(&item, "fee_date", "fee_brief", "fee_amount", "sign_name", "paid_amount")
	return err
}

//DeleteFeeByFeeNo 删除费用
func DeleteFeeByFeeNo(feeNo string) error {
	item := SupplierFeeItem{FeeNo: feeNo}
	_, err := orm.NewOrm().Delete(&item)
	return err
}
