
<div class="print-box clearfix {{if .PrintOne}}print-one{{else}}print-twice{{end}}">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12">
            <h3>百润超市对账汇付单</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-xs-6">
            <b class="text-primary">供应商：</b>{{.PayInfo.Supcust.SupcustNo}}[{{.PayInfo.Supcust.SupName}}]
        </div>
        <div class="col-lg-6 col-md-6 col-xs-6">
            <b class="text-primary">单据号：</b>{{.PayInfo.SheetNo}}
        </div>
    </div>
    <div class="row" style="border-bottom:1px solid #e8e8e8">
        <div class="col-lg-6 col-md-6 col-md-12">
            <b class="text-primary">分支机构：</b>{{.Dept}}
        </div>
        <div class="col-lg-6 col-md-6 hidden-xs">
            <b class="text-primary">操作员：</b>{{.PayInfo.OperName}}
        </div>
    </div>
    <div class="row payment-detail">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12">
                    <section class="text-muted text-center">对账货单明细</section>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-xs-6" style="padding-right:0;border-right:1px solid #e8e8e8">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>编号</th>
                                <th>日期</th>
                                <th>类型</th>
                                <th>金额</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{str2html .DetailHtml1}}
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-6" style="padding-left:0;border-left:1px solid #e8e8e8">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>编号</th>
                                <th>日期</th>
                                <th>类型</th>
                                <th>金额</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{str2html .DetailHtml2}}
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row" style="border-bottom:1px solid #e8e8e8">
                <div class="col-lg-12">
                    <b class="text-danger">单据合计：</b>{{.TotalSheetAmount}}
                </div>
            </div>
        </div>
    </div>
    <div class="row fee-detail" style="border-bottom:1px solid #e8e8e8">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12">
                    <section class="text-muted text-center">费用明细</section>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>编号</th>
                                <th>日期</th>
                                <th>明细</th>
                                <th>费用金额</th>
                                <th>申请人</th>
                                <th>抵扣金额</th>
                                <th class="hidden-print">操作<button type="button" class="btn btn-xs btn-success" id="add_fee"><i class="glyphicon glyphicon-plus"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            {{range $i,$v := .PayInfo.FeeDetail}}
                            <tr>
                                <td>{{$v.FeeNo}}</td>
                                <td>{{dateformat $v.FeeDate "2006-01-02"}}</td>
                                <td>{{$v.FeeBrief}}</td>
                                <td>{{$v.FeeAmount}}</td>
                                <td>{{$v.SignName}}</td>
                                <td>{{$v.PaidAmount}}</td>
                                <td class="hidden-print">
                                    <button type="button" class="btn btn-xs btn-primary edit-fee" fee-index="{{$i}}"><div class="glyphicon glyphicon-pencil"></div></button>
                                    <button type="button" class="btn btn-xs btn-danger delete-fee" fee-index="{{$i}}"><div class="glyphicon glyphicon-trash"></div></button>
                                </td>
                            </tr>
                            {{end}}
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <b class="text-success">费用合计：</b><i class="total-fee-amount">{{.FeeAmount}}</i>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5 col-md-5 col-xs-5">
            <b class="text-primary">开户行/户名：</b>{{.PayInfo.Supcust.SupCashBankMan}}
        </div>
        <div class="col-lg-7 col-md-7 col-xs-7">
            <b class="text-primary">银行账号：</b>{{.PayInfo.Supcust.SupCashBankNo}}
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5 col-md-5 col-xs-5">
            <b class="text-primary">应付账款：</b><i class="rmb-low">{{.LastAmount}}</i>
        </div>
        <div class="col-lg-7 col-md-7 col-xs-7">
            <b class="text-primary">应付大写：</b><i class="rmb-up"></i>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <p class="text-center">对账：</p>
            <p class="text-right">对账日期：{{dateformat .PayInfo.OperDate "2006-01-02"}}</p>
        </div>
    </div>
    <div class="row text-right">
        <i>备注:以上数据如有错误请在五天以内告知，以便更正，谢谢！</i>
    </div>
</div>
<!--第二次-->
<div class="print-box clearfix visible-print {{if .PrintOne}}print-one{{else}}print-twice{{end}}" style="margin-top:10mm;">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12">
            <h3>百润超市对账汇付单</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-xs-6">
            <b class="text-primary">供应商：</b>{{.PayInfo.Supcust.SupcustNo}}[{{.PayInfo.Supcust.SupName}}]
        </div>
        <div class="col-lg-6 col-md-6 col-xs-6">
            <b class="text-primary">单据号：</b>{{.PayInfo.SheetNo}}
        </div>
    </div>
    <div class="row" style="border-bottom:1px solid #e8e8e8">
        <div class="col-lg-6 col-md-6 col-md-12">
            <b class="text-primary">分支机构：</b>{{.Dept}}
        </div>
        <div class="col-lg-6 col-md-6 hidden-xs">
            <b class="text-primary">操作员：</b>{{.PayInfo.OperName}}
        </div>
    </div>
    <div class="row payment-detail">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12">
                    <section class="text-muted text-center">对账货单明细</section>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-xs-6" style="padding-right:0;border-right:1px solid #e8e8e8">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>编号</th>
                                <th>日期</th>
                                <th>类型</th>
                                <th>金额</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{str2html .DetailHtml1}}
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-6" style="padding-left:0;border-left:1px solid #e8e8e8">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>编号</th>
                                <th>日期</th>
                                <th>类型</th>
                                <th>金额</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{str2html .DetailHtml2}}
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row" style="border-bottom:1px solid #e8e8e8">
                <div class="col-lg-12">
                    <b class="text-danger">单据合计：</b>{{.TotalSheetAmount}}
                </div>
            </div>
        </div>
    </div>
    <div class="row fee-detail" style="border-bottom:1px solid #e8e8e8">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12">
                    <section class="text-muted text-center">费用明细</section>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>编号</th>
                                <th>日期</th>
                                <th>明细</th>
                                <th>费用金额</th>
                                <th>申请人</th>
                                <th>抵扣金额</th>
                                <th class="hidden-print">操作<button type="button" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-plus"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            {{range $i,$v := .PayInfo.FeeDetail}}
                            <tr>
                                <td>{{$v.FeeNo}}</td>
                                <td>{{dateformat $v.FeeDate "2006-01-02"}}</td>
                                <td>{{$v.FeeBrief}}</td>
                                <td>{{$v.FeeAmount}}</td>
                                <td>{{$v.SignName}}</td>
                                <td>{{$v.PaidAmount}}</td>
                                <td class="hidden-print">
                                    <button type="button" class="btn btn-xs btn-primary edit-fee" fee-index="{{$i}}"><div class="glyphicon glyphicon-pencil"></div></button>
                                    <button type="button" class="btn btn-xs btn-danger delete-fee" fee-index="{{$i}}"><div class="glyphicon glyphicon-trash"></div></button>
                                </td>
                            </tr>
                            {{end}}
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <b class="text-success">费用合计：</b><i class="total-fee-amount">{{.FeeAmount}}</i>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5 col-md-5 col-xs-5">
            <b class="text-primary">开户行/户名：</b>{{.PayInfo.Supcust.SupCashBankMan}}
        </div>
        <div class="col-lg-7 col-md-7 col-xs-7">
            <b class="text-primary">银行账号：</b>{{.PayInfo.Supcust.SupCashBankNo}}
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5 col-md-5 col-xs-5">
            <b class="text-primary">应付账款：</b><i class="rmb-low">{{.LastAmount}}</i>
        </div>
        <div class="col-lg-7 col-md-7 col-xs-7">
            <b class="text-primary">应付大写：</b><i class="rmb-up"></i>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <p class="text-center">对账：</p>
            <p class="text-right">对账日期：{{dateformat .PayInfo.OperDate "2006-01-02"}}</p>
        </div>
    </div>
    <div class="row text-right">
        <i>备注:以上数据如有错误请在五天以内告知，以便更正，谢谢！</i>
    </div>
</div>

<div class="clearfix hidden-print">
    <div class="row">
        <div class="col-lg-4 col-lg-offset-4">
            {{if .ApproveFlag}}
                <button class="btn btn-sm btn-primary" onclick="print()">打印</button>
            {{else}}
                {{if .BranchPass}}
                    <button class="btn btn-sm btn-warning" disabled="disabled">
                        未审核
                    </button>
                {{else}}
                    <button class="btn btn-sm btn-danger" disabled="disabled">
                        单据不对
                    </button>
                {{end}}
            {{end}}
            <a class="btn btn-sm btn-default" href="/admin/sup/pay_list?supcust_no={{.PayInfo.Supcust.SupcustNo}}">返回</a>
        </div>
    </div>
</div>

<!-- fee modal -->
<div class="modal fade" id="feeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">增加扣减费用</h4>
      </div>
      <div class="modal-body">
        <form>
            <div class="form-group">
                <label for="FeeDate">费用日期</label>
                <input type="text" class="form-control" id="FeeDate">
            </div>
            <div class="form-group">
                <label for="FeeBrief">费用明细</label>
                <input type="text" class="form-control" id="FeeBrief">
            </div>
            <div class="form-group">
                <label for="FeeAmount">费用金额</label>
                <input type="text" class="form-control" id="FeeAmount">
            </div>
            <div class="form-group">
                <label for="SignName">申请人</label>
                <input type="text" class="form-control" id="SignName">
            </div>
        </form>
      </div>
      <div class="modal-footer">
	        <button type="button" class="btn btn-primary" id="save_fee">保存</button>
	        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
      </div>
    </div>
  </div>
</div>
