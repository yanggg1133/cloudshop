<!DOCTYPE html>

<html>
    <head>
        <title>Beego</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    
    <body>
        <header class="hero-unit" style="background-color:#A9F16C">
            <div class="container">
            <div class="row">
              <div class="hero-text">
                <ul>
                    {{range $k,$v := .users}}
                    <li>
                        {{$v.UserName}}
                        <ul>
                            {{range $d,$t := $v.Checks}}
                            <li>{{$d}} => {{$t.CheckInTime}}</li>
                            {{end}}
                        </ul>
                    </li>
                    {{end}}
                </ul>
              </div>
            </div>
            </div>
        </header>
    </body>
</html>