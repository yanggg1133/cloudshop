
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>后台管理</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" href="/static/css/bootstrap-blue.css">
    <link rel="stylesheet" href="/static/css/bootstrap-blue-custom.css">
    <link rel="stylesheet" href="/static/css/admin-payment-print.css" media="print">
  </head>
  <body>
    <div class="navbar navbar-default navbar-fixed-top hidden-print">
      <div class="container">
        <div class="navbar-header">
          <a href="./index" class="navbar-brand">内部管理子系统</a>
          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
          <ul class="nav navbar-nav">

          </ul>

          <ul class="nav navbar-nav navbar-right">
            
          </ul>

        </div>
      </div>
    </div>

    <div class="container">
      <!--Layout Content starts-->
      {{.LayoutContent}}
      <!--Layout ends-->
    </div>
    
    <script src="/static/js/jquery-1.10.2.min.js"></script>
    <script src="/static/bootstrap/dist/js/bootstrap.min.js"></script>
    {{.JavaScriptContent}}

</html>
