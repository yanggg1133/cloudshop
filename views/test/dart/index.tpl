<!DOCTYPE html>
<html>
  <head>
    <title>Getting Started</title>
    <link rel="stylesheet" href="/static/css/styles.css">
    <script async src="/static/angular2/build/web/main.dart.js"></script>
  </head>
  <body>
    <my-app>Loading...</my-app>
  </body>
</html>