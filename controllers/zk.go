package controllers

import (
	"github.com/astaxie/beego"
)

//ZKPreparer define the infine the interface of zktimes router
type ZKPreparer interface {
	ZKPrepare()
}

//ZKBase implements ControllerInterface
type ZKBase struct {
	beego.Controller
}

//Prepare get ready to run interface
func (zk *ZKBase) Prepare() {
	if app, ok := zk.AppController.(ZKPreparer); ok {
		app.ZKPrepare()
	}
}
