package sup

import (
	"git.oschina.net/tinkler/cloudshop/controllers"
)

//ReconciliationController implement SupplierRouter 对账控制器
type ReconciliationController struct {
	controllers.SupplierBase
}

//URLMapping mapping the url
func (rc *ReconciliationController) URLMapping() {
	rc.Mapping("Index", rc.Index)
}

//Index of ReconciliationController
//@router /reconciliation/index [get]
func (rc *ReconciliationController) Index() {
	rc.TplName = `index.tpl`
}
