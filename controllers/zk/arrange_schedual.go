package zk

import (
	"git.oschina.net/tinkler/cloudshop/controllers"
	"git.oschina.net/tinkler/cloudshop/models/zktimes"
)

//ArrangeSchedualController implement ZKRouter
type ArrangeSchedualController struct {
	controllers.ZKBase
}

//URLMapping mapping the url
func (asc *ArrangeSchedualController) URLMapping() {
	asc.Mapping("Index", asc.Index)
}

//Index of ArrangeSchedualController
//@router /as/index [get]
func (asc *ArrangeSchedualController) Index() {
	asc.TplName = `zk/as/index.tpl`
}

//Results of ArrangeSchedualController
//@router /as/results
func (asc *ArrangeSchedualController) Results() {
	asc.Data[`json`] = zktimes.GetUsers()
	asc.ServeJSON()
}
