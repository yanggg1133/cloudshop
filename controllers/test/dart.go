package test

import (
	"git.oschina.net/tinkler/cloudshop/controllers"
)

//DartController implement TestRouter
type DartController struct {
	controllers.TestBase
}

//URLMapping mapping the url
func (dc *DartController) URLMapping() {
	dc.Mapping("Index", dc.Index)
}

//Index of DartController
//@router /dart/index [get]
func (dc *DartController) Index() {
	dc.TplName = `test/dart/index.tpl`
}
