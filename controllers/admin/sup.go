package admin

import (
	"strconv"
	"strings"
	"time"

	"git.oschina.net/tinkler/cloudshop/controllers"
	"git.oschina.net/tinkler/cloudshop/models/kmjxc"
)

//SupController implement AdminBase 供应商管理控制器
type SupController struct {
	controllers.AdminBase
}

//URLMapping mapping the url
func (sc *SupController) URLMapping() {
	sc.Mapping("Index", sc.Index)
}

//Index method of SupController
//@router /sup/index [get]
func (sc *SupController) Index() {
	sc.TplName = `admin/sup/index.tpl`
	sc.Data["SupcustInfo"] = kmjxc.GetBiTSupcupstInfo("%")
}

//Fee 供应商费用管理
//@router /sup/fee [get]
func (sc *SupController) Fee() {
	sc.TplName = `admin/sup/fee.tpl`
}

//PayList 付款单列表
//@router /sup/pay_list [get]
func (sc *SupController) PayList() {
	supcustNo := sc.GetString("supcust_no")
	if supcustNo == "" {
		supcustNo = "010003"
	}
	sc.TplName = `admin/sup/pay_list.tpl`
	sc.Data["PaymentList"] = kmjxc.GetRpTRecpayRecordInfoListBySupcustNo(supcustNo)
}

//PayDetail 付款单
//@router /sup/pay_detail [get]
func (sc *SupController) PayDetail() {
	sheetNo := sc.GetString("sheet_no")
	if sheetNo == "" {
		sheetNo = "RP001606160215"
	}
	sc.TplName = `admin/sup/pay_detail.tpl`
	info := kmjxc.GetRpTRecpayRecordInfoBySheetNo(sheetNo)
	sc.Data[`PayInfo`] = info
	detail1 := ""
	detail2 := ""
	amount := 0.0
	branchNo := ""
	approveFlag := info.ApproveFlag
	branchPass := true
	for i, v := range info.Detail {
		amount += v.SheetAmount
		if branchNo != "" && branchNo != v.SettleNo[2:4] {
			branchPass = false
		} else {
			branchNo = v.SettleNo[2:4]
		}
		var tp string
		if v.SettleNo[0:2] == "KJ" {
			tp = "进价调整"
		} else if v.SettleNo[0:2] == "RO" {
			tp = "采购退货"
		} else {
			tp = "验收入库"
		}
		if i < len(info.Detail)/2+len(info.Detail)%2 {
			detail1 += "<tr><td>" + v.SettleNo + "</td><td>" + v.PaidDate.Format("2006-01-02") + "</td><td>" + tp + "</td><td>" + strconv.FormatFloat(v.SheetAmount, 'f', 2, 64) + "</td></tr>"
		} else {
			detail2 += "<tr><td>" + v.SettleNo + "</td><td>" + v.SettleDate.Format("2006-01-02") + "</td><td>" + tp + "</td><td>" + strconv.FormatFloat(v.SheetAmount, 'f', 2, 64) + "</td></tr>"
		}
	}
	sc.Data["DetailHtml1"] = detail1
	sc.Data["DetailHtml2"] = detail2
	sc.Data["TotalSheetAmount"] = strconv.FormatFloat(amount, 'f', 2, 64)
	sc.Data["ApproveFlag"] = approveFlag
	sc.Data["BranchPass"] = branchPass

	feeAmount := 0.0
	for _, v := range info.FeeDetail {
		feeAmount -= v.PaidAmount
	}
	sc.Data["FeeAmount"] = strconv.FormatFloat(feeAmount, 'f', 0, 64)

	lastAmount := amount + feeAmount
	sc.Data["LastAmount"] = strconv.FormatFloat(lastAmount, 'f', 2, 64)

	if branchNo == "00" {
		sc.Data[`Dept`] = "盛世东方店"
	} else {
		sc.Data[`Dept`] = "新兴店"
	}
	if len(info.Detail)+len(info.FeeDetail) <= 13 {
		sc.Data[`PrintOne`] = true
	} else {
		sc.Data[`PrintOne`] = false
	}
	sc.LayoutSections = make(map[string]string)
	sc.LayoutSections["JavaScriptContent"] = `admin/sup/pay_detail.script.tpl`
}

//SaveFee 保存费用增加，修改
//@router /sup/save_fee [post]
func (sc *SupController) SaveFee() {
	var errs []string
	action := sc.GetString("action")
	if action == "add" {
		feeDate, err := time.Parse("2006-01-02", sc.GetString("fee_date"))
		if err != nil {
			errs = append(errs, "费用日期格式错误！")

		}
		feeBrief := sc.GetString("fee_brief")
		feeAmount, err := strconv.ParseFloat(sc.GetString("fee_amount"), 64)
		if err != nil {
			errs = append(errs, "费用金额错误")
		}
		signName := sc.GetString("sign_name")
		sheetNo := sc.GetString("sheet_no")
		if len(sheetNo) != 14 && sheetNo[0:2] != "RP" {
			errs = append(errs, "汇付单编码错误！")
		}
		supcustNo := strings.TrimSpace(sc.GetString("supcust_no"))
		if len(supcustNo) != 6 {
			errs = append(errs, "供应商编号错误！")
		}
		dept := sc.GetString("dept")
		if dept == "" {
			errs = append(errs, "分支机构错误！")
		}
		if dept == "新兴店" {
			dept = "03"
		} else if dept == "盛世东方店" {
			dept = "00"
		}
		if len(errs) > 0 {
			sc.Data["json"] = map[string]interface{}{"result": false, "messages": errs}
		} else {
			feeNo, err := kmjxc.AddFeeOnRpTRecpayRecordInfo(feeDate, feeBrief, feeAmount, signName, sheetNo, supcustNo, dept)
			if err != nil {
				errs = append(errs, "保存失败！")
				sc.Data["json"] = map[string]interface{}{"result": false, "messages": errs}
			} else {
				sc.Data["json"] = map[string]interface{}{"result": true, "fee_no": feeNo}
			}
		}
	} else if action == "edit" {
		feeDate, err := time.Parse("2006-01-02", sc.GetString("fee_date"))
		if err != nil {
			errs = append(errs, "费用日期格式不正确！")
		}
		feeBrief := sc.GetString("fee_brief")
		feeAmount, err := strconv.ParseFloat(sc.GetString("fee_amount"), 64)
		if err != nil {
			errs = append(errs, "费用金额格式不正确!")
		}
		signName := sc.GetString("sign_name")
		feeNo := sc.GetString("fee_no")
		if feeNo == "" {
			errs = append(errs, "费用编码错误")
		}
		if len(errs) > 0 {
			sc.Data["json"] = map[string]interface{}{"result": false, "messages": errs}
		} else {
			err = kmjxc.UpdateFeeByFeeNo(feeNo, feeDate, feeBrief, feeAmount, signName)
			if err != nil {
				errs = append(errs, "保存失败！")
				sc.Data["json"] = map[string]interface{}{"result": false, "messages": errs}
			} else {
				sc.Data["json"] = map[string]interface{}{"result": true, "fee_no": feeNo}
			}
		}
	} else if action == "delete" {
		feeNo := sc.GetString("fee_no")
		if feeNo == "" {
			errs = append(errs, "费用编号错误")
		}
		if len(errs) > 0 {
			sc.Data["json"] = map[string]interface{}{"result": false, "messages": errs}
		} else {
			err := kmjxc.DeleteFeeByFeeNo(feeNo)
			if err != nil {
				errs = append(errs, "保存失败")
				sc.Data["json"] = map[string]interface{}{"result": false, "messages": errs}
			} else {
				sc.Data["json"] = map[string]interface{}{"result": true, "fee_no": feeNo}
			}
		}
	}

	sc.ServeJSON()
}

//SpeSold 特价销售 查看
//@router /sup/spe_sold [get]
func (sc *SupController) SpeSold() {
	sc.TplName = `admin/sup/spe_sold.tpl`
}
