package admin

import "git.oschina.net/tinkler/cloudshop/controllers"

type StoController struct {
	controllers.AdminBase
}

//Check method of StoController
//@router /sto/check
func (sc *StoController) Check() {
	sc.TplName = `admin/sto/check.tpl`
}
