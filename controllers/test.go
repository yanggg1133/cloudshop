package controllers

import (
	"github.com/astaxie/beego"
)

//TestPreparer define the interface of test router
type TestPreparer interface {
	TestPrepare()
}

//TestBase implement ControllerInterface
type TestBase struct {
	beego.Controller
}

//Prepare get ready to run interface
func (t *TestBase) Prepare() {
	if app, ok := t.AppController.(TestPreparer); ok {
		app.TestPrepare()
	}
}
