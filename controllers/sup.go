package controllers

import (
	"github.com/astaxie/beego"
)

//SupplierPreparer define the interface of supplier router
type SupplierPreparer interface {
	SupplierPrepare()
}

//SupplierBase implements ControllerInterface
type SupplierBase struct {
	beego.Controller
}

//Prepare get ready to run interface
func (sup *SupplierBase) Prepare() {
	if app, ok := sup.AppController.(SupplierPreparer); ok {
		app.SupplierPrepare()
	}
}

//Login 供应商对账登录
func (sup *SupplierBase) Login(){
	
}