package routers

import (
	"github.com/astaxie/beego"
)

func init() {

	beego.GlobalControllerRouter["git.oschina.net/tinkler/cloudshop/controllers/zk:ArrangeSchedualController"] = append(beego.GlobalControllerRouter["git.oschina.net/tinkler/cloudshop/controllers/zk:ArrangeSchedualController"],
		beego.ControllerComments{
			Method: "Index",
			Router: `/as/index`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/tinkler/cloudshop/controllers/zk:ArrangeSchedualController"] = append(beego.GlobalControllerRouter["git.oschina.net/tinkler/cloudshop/controllers/zk:ArrangeSchedualController"],
		beego.ControllerComments{
			Method: "Results",
			Router: `/as/results`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/tinkler/cloudshop/controllers/zk:CheckController"] = append(beego.GlobalControllerRouter["git.oschina.net/tinkler/cloudshop/controllers/zk:CheckController"],
		beego.ControllerComments{
			Method: "Index",
			Router: `/check/index`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

}
