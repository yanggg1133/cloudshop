package routers

import (
	"github.com/astaxie/beego"
)

func init() {

	beego.GlobalControllerRouter["git.oschina.net/tinkler/cloudshop/controllers/sup:ReconciliationController"] = append(beego.GlobalControllerRouter["git.oschina.net/tinkler/cloudshop/controllers/sup:ReconciliationController"],
		beego.ControllerComments{
			Method: "Index",
			Router: `/reconciliation/index`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

}
