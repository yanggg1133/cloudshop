package routers

import (
	"github.com/astaxie/beego"
)

func init() {

	beego.GlobalControllerRouter["spaceshop/controllers/admin:StoController"] = append(beego.GlobalControllerRouter["spaceshop/controllers/admin:StoController"],
		beego.ControllerComments{
			"Check",
			`/sto/check`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["spaceshop/controllers/admin:SupController"] = append(beego.GlobalControllerRouter["spaceshop/controllers/admin:SupController"],
		beego.ControllerComments{
			"Index",
			`/sup/index`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["spaceshop/controllers/admin:SupController"] = append(beego.GlobalControllerRouter["spaceshop/controllers/admin:SupController"],
		beego.ControllerComments{
			"Fee",
			`/sup/fee`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["spaceshop/controllers/admin:SupController"] = append(beego.GlobalControllerRouter["spaceshop/controllers/admin:SupController"],
		beego.ControllerComments{
			"PayList",
			`/sup/pay_list`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["spaceshop/controllers/admin:SupController"] = append(beego.GlobalControllerRouter["spaceshop/controllers/admin:SupController"],
		beego.ControllerComments{
			"PayDetail",
			`/sup/pay_detail`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["spaceshop/controllers/admin:SupController"] = append(beego.GlobalControllerRouter["spaceshop/controllers/admin:SupController"],
		beego.ControllerComments{
			"SaveFee",
			`/sup/save_fee`,
			[]string{"post"},
			nil})

	beego.GlobalControllerRouter["spaceshop/controllers/admin:SupController"] = append(beego.GlobalControllerRouter["spaceshop/controllers/admin:SupController"],
		beego.ControllerComments{
			"SpeSold",
			`/sup/spe_sold`,
			[]string{"get"},
			nil})

}
