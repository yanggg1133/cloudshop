package routers

import (
	"github.com/astaxie/beego"
)

func init() {

	beego.GlobalControllerRouter["git.oschina.net/tinkler/cloudshop/controllers/test:DartController"] = append(beego.GlobalControllerRouter["git.oschina.net/tinkler/cloudshop/controllers/test:DartController"],
		beego.ControllerComments{
			Method: "Index",
			Router: `/dart/index`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

}
